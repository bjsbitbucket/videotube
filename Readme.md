Sample app to load videos to firebase storage, and display videos in recycler view, then watch on clicking on them..
Commentes are provided on each file to show their purpose.

Task: 
1. A video compressor to be implemented to compress videos before uploading. 
2. Exoplayer to be implemented with buffering of the videos. Exoplayer give facility to stop and resume videos along with to turn up/down volume.
As of now default media player is playing the video. Sample Exoplayer code is given.


Hint:
Video compressor to be implemented in MainAcivity at line 128
