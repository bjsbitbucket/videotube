package com.freelancer.videocompressor;
/*
 This is the model to hold video information
 */
public class VideoDetails
{
    private String link;
    private String name;
    private String size;

    public VideoDetails()
    {

    }
    public VideoDetails(String link, String name, String size) {
        this.link = link;
        this.name = name;
        this.size = size;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
