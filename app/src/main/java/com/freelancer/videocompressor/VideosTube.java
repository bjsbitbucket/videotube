package com.freelancer.videocompressor;
/*
   Activity to display video list in recycler view
 */
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;
import java.util.List;

public class VideosTube extends AppCompatActivity {

    private final List<VideoDetails> videosList = new LinkedList<>();
    private LinearLayoutManager linearLayoutManager;
    private VideoDisplayAdapter videoDisplayAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_tube2);
        initializeUiItems();
    }

    private void initializeUiItems()
    {
        recyclerView = findViewById(R.id.videos_list);
        linearLayoutManager = new LinearLayoutManager(this);
        videoDisplayAdapter = new VideoDisplayAdapter(videosList, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(videoDisplayAdapter);

        populateVideos();
    }

    private void populateVideos()
    {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("videos");
        databaseReference
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists() && dataSnapshot.hasChildren())
                        {
                            for(DataSnapshot ds : dataSnapshot.getChildren())
                            {
                                VideoDetails videoDetails = ds.getValue(VideoDetails.class);
                                videosList.add(videoDetails);
                            }

                            if(videosList.size() > 0)
                            {
                                recyclerView.getAdapter().notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
