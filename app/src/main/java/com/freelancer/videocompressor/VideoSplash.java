package com.freelancer.videocompressor;
/*
   VideoSplash to play video from firebase link
 */
import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.VideoView;

public class VideoSplash extends AppCompatActivity implements MediaPlayer.OnCompletionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_splash);

        VideoView video = (VideoView) findViewById(R.id.videoView);
        String videoLink = "";
        if(getIntent().getExtras().get("videolink") != null)
        {
            videoLink = getIntent().getExtras().get("videolink").toString();
        }

        video.setVideoPath(videoLink);
        video.start();
        video.getBufferPercentage();
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
//                loading.dismiss();
            }
        });



        video.setOnCompletionListener(this);
        video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                finish();
                return false;
            }
        });
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        finish();
    }
}
