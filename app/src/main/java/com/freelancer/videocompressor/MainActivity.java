package com.freelancer.videocompressor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

    private Button sendFile, go_to_video_list;
    private static final int RC_PHOTO_PICKER_PERM = 555;
    private static final int RequestCodeForVideoPick = 1;
    private ProgressBar progressBar;
    private UploadTask uploadTask;
    private DatabaseReference videoDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sendFile = findViewById(R.id.send_file);
        progressBar = findViewById(R.id.groupMessageProgressBar);
        sendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                pickVideoClicked();
            }
      });
        videoDatabaseReference = FirebaseDatabase.getInstance().getReference("videos");
        go_to_video_list = findViewById(R.id.watch_videos);
        go_to_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, VideosTube.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @AfterPermissionGranted(RC_PHOTO_PICKER_PERM)
    public void pickVideoClicked() {
        if (EasyPermissions.hasPermissions(this, FilePickerConst.PERMISSIONS_FILE_PICKER)) {
            FilePickerBuilder.getInstance()
                    .setActivityTitle("Please select video")
                    .enableVideoPicker(true)
                    .enableCameraSupport(false)
                    .setMaxCount(1)
                    .showFolderView(true)
                    .enableImagePicker(false)
                    .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .pickPhoto(this, RequestCodeForVideoPick);

        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_photo_picker),
                    RC_PHOTO_PICKER_PERM, FilePickerConst.PERMISSIONS_FILE_PICKER);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ArrayList<Uri> dataList = null;
        dataList = data.getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);

        for (final Uri resultUri : dataList) {

            final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("MessageMedia").child("Video file");
            final File file = new File(resultUri.getPath());
            final String name = getFileName(resultUri);

            final DatabaseReference vdoDbReference = videoDatabaseReference.push();
            final String messagePushID = vdoDbReference.getKey();
            final StorageReference filePath = storageReference.child(messagePushID + "_" + name);
            final DatabaseReference vidoeRef = videoDatabaseReference;
            /*
              Compressor should be here so that file can be compressed before staring the upload task.
             */
            uploadTask = filePath.putFile(resultUri);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(20);

            final double filesize[] = new double[1];

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    filesize[0]  = taskSnapshot.getTotalByteCount();

                    float progress = (float) (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    int currentprogress = (int) progress;

                    progressBar.setProgress(currentprogress);
                }
            }).continueWithTask(new Continuation() {

                @Override
                public Object then(@NonNull Task task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
//
                        final Uri downloadUrl = task.getResult();
                        String myUrl = downloadUrl.toString();

                        String sizeinstring = String.valueOf(filesize[0]/(1024 * 1024));
                        String[] twoparts = sizeinstring.split("\\.");
                        if(twoparts.length > 1 && twoparts[1] != null && twoparts[1].length() > 2)
                        {
                            sizeinstring = twoparts[0] +"."+ twoparts[1].substring(0, 2);
                        }
                        VideoDetails videoDetails = new VideoDetails(myUrl, name,  sizeinstring + " MB");
                        Map messageBodyDetails = new HashMap();
                        messageBodyDetails.put( messagePushID, videoDetails);
                        vidoeRef
                                .updateChildren(messageBodyDetails)
                                .addOnCompleteListener(new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task)
                                    {
                                        if(task.isSuccessful())
                                        {
                                            Toast.makeText(MainActivity.this, "A new video added", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Upload failed" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
        }


    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }



}
