package com.freelancer.videocompressor;
/*
  Video adapter to display videos in recycler view.
 */
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class VideoDisplayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<VideoDetails> videoDetailsList;
    private Context context;

    public VideoDisplayAdapter(List<VideoDetails> videoDetailsList, Context context) {
        this.videoDetailsList = videoDetailsList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_video_display_layout, parent, false);


        return new VideoDisplayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final String videoName = videoDetailsList.get(position).getName();
        final String link = videoDetailsList.get(position).getLink();
        final String size = videoDetailsList.get(position).getSize();
        VideoDisplayViewHolder videoDisplayViewHolder = (VideoDisplayViewHolder)holder;

        // To display an image icon of a video
        Glide.with(context)
                .asBitmap()
                .load(link != null ? link : "")
                .into(videoDisplayViewHolder.video_icon);
        videoDisplayViewHolder.videoDetails.setText(videoName + " Size: " + size);

        //Not a good way to add listener.. will be fixed later
        //Start playing a video on clicking the video icon
        videoDisplayViewHolder.video_icon.
                setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent playMusi = new Intent(context, VideoSplash.class);
                if(link != null)
                {
                    playMusi.putExtra("videolink", link);
                    context.startActivity(playMusi);
                }
                else
                {
                    Toast.makeText(context, "Invalid Video link", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return videoDetailsList != null ? videoDetailsList.size() : 0;
    }

    public class VideoDisplayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public ImageButton video_icon;
        public TextView videoDetails;

        public VideoDisplayViewHolder(@NonNull View itemView) {
            super(itemView);
            video_icon = itemView.findViewById(R.id.video_icon);
            videoDetails = itemView.findViewById(R.id.video_icon_details);

        }

        @Override
        public void onClick(View v)
        {
            Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
        }
    }
}
